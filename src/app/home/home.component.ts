import { Component, OnInit } from '@angular/core';
import { ConfigComponent } from '../config/config.component';
import { PanelService } from '../panel.service';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public config: ConfigService,
    private panel: PanelService
  ) { }

  ngOnInit(): void {
  }

  isConfigured() {
    return this.config.isConfigured();
  }

  openConfig() {
    ConfigComponent.open(this.panel, {});
  }
}
