import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel-info',
  templateUrl: './panel-info.component.html',
  styleUrls: ['./panel-info.component.scss']
})
export class PanelInfoComponent implements OnInit {
  @Input() title;
  @Input() description;
  @Input() classes;
  @Input() icon;

  static ID = Symbol();
  constructor() { }

  ngOnInit() {
  }
}
