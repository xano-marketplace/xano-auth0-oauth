import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-oauth-auth0',
  templateUrl: './oauth-auth0.component.html',
  styleUrls: ['./oauth-auth0.component.scss']
})
export class OauthAuth0Component implements OnInit {
  authenticating: any;
  redirect_uri: string;
  apiUrl;
  mode;
  result;

  callback = (e) => {
    if (e && e["origin"] == document.location.origin && e["data"] && e["data"]["type"] == `oauth:auth0`) {
      this.authenticating = true;
      this.api.get({
        endpoint: this.apiUrl,
        params: {
          code: e["data"]["args"]["code"],
          redirect_uri: this.redirect_uri,
        },
      }).subscribe((response: any) => {
        this.result = response;
        window.removeEventListener('message', this.callback);
      }, err => {
        this.authenticating = false;
        window.removeEventListener('message', this.callback);
        this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      });
    }
  };

  constructor(
    private api: ApiService,
    private toast: ToastService,
    public config: ConfigService
  ) {
    const baseUrl = document.location.origin;
    this.redirect_uri = baseUrl.includes('localhost') ? baseUrl + '/assets/oauth/auth0/index.html' :
      baseUrl + '/xano-auth0-oauth/assets/oauth/auth0/index.html';
  }

  ngOnInit(): void {
  }

  initOAuth(mode, endpoint) {
    this.mode = mode;
    this.apiUrl = endpoint;

    let oauthWindow = window.open("", `auth0Oauth`, `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=600,height=660,left=100,top=100`);

    window.removeEventListener('message', this.callback);
    window.addEventListener('message', this.callback);

    this.api.get({
      endpoint: `${this.config.xanoApiUrl}/oauth/auth0/init`,
      params: {
        redirect_uri: this.redirect_uri
      }
    }).subscribe((response: any) => {
      oauthWindow.location.href = response.authUrl;
    }, err => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      oauthWindow.close();
    });
  }

  continueOAuthAuth0() {
    this.initOAuth('continue', `${this.config.xanoApiUrl}/oauth/auth0/continue`);
  }

  logout() {
    this.result = false;
    this.mode = '';
  }

  getRedirectURI() {
    return this.redirect_uri;
  }
}
